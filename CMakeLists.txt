cmake_minimum_required(VERSION 2.8)
project(avidbots_camera_merge)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

catkin_package(
  INCLUDE_DIRS include
  CATKIN_DEPENDS roscpp
  CATKIN_DEPENDS cv_bridge image_transport message_filters sensor_msgs roslint nodelet
  DEPENDS OpenCV
  LIBRARIES ${PROJECT_NAME}
)

find_package(Boost REQUIRED)
find_package(OpenCV 2.4.13.2 REQUIRED)
find_package(catkin REQUIRED cv_bridge image_transport message_filters sensor_msgs roslint nodelet)

roslint_cpp()

set(OpenCV_DIR "/usr/local/share/OpenCV/")
MESSAGE( STATUS "OpenCV Lib:   ${OpenCV_VERSION}    ${OpenCV_LIB_DIR_OPT} ${OpenCV_LIB_DIR_DBG} ")

include_directories(include)
include_directories(${catkin_INCLUDE_DIRS} ${OpenCV_INCLUDE_DIRS})

# add the subscriber example
add_executable(image_streamer src/image_streamer.cpp)
add_executable(video_recorder_node src/video_recorder_node.cpp)

add_library(${PROJECT_NAME} 
  src/video_recorder_nodelet.cpp
  src/video_recorder.cpp
)
target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBRARIES} ${catkin_LIBRARIES} ${Boost_LIBRARIES})

target_link_libraries(image_streamer ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})
target_link_libraries(video_recorder_node ${OpenCV_LIBRARIES} ${PROJECT_NAME} ${catkin_LIBRARIES} ${Boost_LIBRARIES})


#############
## Install ##
#############

install(TARGETS
    image_streamer
    video_recorder_node
    DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(TARGETS ${PROJECT_NAME}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
)

install(DIRECTORY
    launch
    param
    DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)

install(DIRECTORY include/${PROJECT_NAME}/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
)

install(FILES nodelet_plugins.xml
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)

