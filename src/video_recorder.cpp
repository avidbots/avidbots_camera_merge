/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2016, Avidbots Corp.
 * @name  video_recorder
 * @brief streams color image and generates/saves a video.
 * @author  Yoohee Choi, Joseph Duchesne
 */

#include <ctime>
#include <string>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <chrono>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/CompressedImage.h>
#include <message_filters/subscriber.h>
#include <nodelet/nodelet.h>

#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include "avidbots_camera_merge/video_recorder.h"


/**
 * @name      Video
 * @brief     Constructor
 * @param[in] path: file path to where the vidoes to be stored.
 */
VideoRecorder::VideoRecorder(const std::string& path, ros::NodeHandle nodehandle_) :
  left_sub_(nodehandle_, "/camera_left/rgb/image_raw", 1),
  right_sub_(nodehandle_, "/camera_right/rgb/image_raw", 1),
  sync_(CameraSyncPolicy(5), left_sub_, right_sub_),
  image_transport_(nodehandle_)
{
  /* Initialize parameters */
  int fps, clip_length;
  nodehandle_.param("/camera_left/driver/color_fps", fps_, 30);
  nodehandle_.param("/camera_right/driver/color_fps", fps, 30);

  if (fps_ != fps)
  {
    ROS_ERROR("VideoRecorder: Camera fps mismatch. left camera's fps: %d,"
              "right camera's fps: %d", fps_, fps);
    ROS_ERROR("VideoRecorder: Exiting without recording videos");
    return;
  }

  nodehandle_.param("/clip_length", clip_length, 30);
  combined_pub_ = image_transport_.advertise("/camera_front", 1);
  last_pub_ =  std::chrono::steady_clock::now();

  ROS_WARN("VideoRecorder: fps: %d, clip_length: %d", fps_, clip_length);
  path_ = path;
  max_frames_ = clip_length * 60 * fps_;
  new_clip_ = true;

  sync_.registerCallback(boost::bind(&VideoRecorder::ImagesCallback, this, _1, _2));
}

/**
 * @name      WriteFrame
 * @brief     Write frame to video
 * @param[in] msg - combined image msg (L/R horzontally concatenated)
 */
void VideoRecorder::WriteFrame(const cv::Mat& img)
{
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now(),
                                        end;
  if (new_clip_)
  {
    num_frames_ = 0;
    new_clip_ = false;
    start_ = std::chrono::steady_clock::now();

    /* Get current time in UTC */
    char buff[100];
    time_t start = time(0);
    strftime(buff, 100, "UTC%Y%m%d_%H%M%S", gmtime(&start));

    /* Set up output */
    std::string filename(buff);
    filename = std::string("appsrc ! videoconvert ! video/x-raw,format=NV12 ! vaapiencode_h264 tune=1 bitrate=6200 rate-control=cbr ! avimux ! filesink location=") + path_ + filename + ".avi";
    output_.open(filename, 0, (double)fps_, img.size(), true);
      
    end = std::chrono::steady_clock::now();

    ROS_DEBUG("VideoRecorder: VideoWriter open: elapsed time in nsec: %ld",
              std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count());
  }

  if (output_.isOpened())
  {
    begin = std::chrono::steady_clock::now();
    num_frames_++;
    output_.write(img);
    end = std::chrono::steady_clock::now();
    ROS_DEBUG("VideoRecorder: VideoWriter write: elapsed time in nsec: %ld",
              std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count());
  }
  else
  {
    ROS_ERROR_THROTTLE(3.0, "VideoRecorder: VideoWriter is not opened.");
  }

  end = std::chrono::steady_clock::now();
  ROS_INFO_THROTTLE(1.0, "VideoRecorder: FPS: %f", 
                    static_cast<double>(num_frames_)/static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(end - start_).count())*1000.0);


  if (num_frames_ > max_frames_)
  {
    new_clip_ = true;
  }
}

/**
 * @name       ImagesCallback
 * @brief      Called by the synchronizer message filter when it sync two images (L/R).
 * @param[in]  left_msg: left camera image
 * @param[in]  right_msg: right camera image
 */
void VideoRecorder::ImagesCallback(const sensor_msgs::ImageConstPtr& left_msg,
                                   const sensor_msgs::ImageConstPtr& right_msg)
{
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now(),
                                          end, fn_start = std::chrono::steady_clock::now();
  cv_bridge::CvImageConstPtr left_cv_ptr, right_cv_ptr;
  cv::Mat output, output2;
  try
  {
    left_cv_ptr = cv_bridge::toCvShare(left_msg, sensor_msgs::image_encodings::BGR8);
    right_cv_ptr = cv_bridge::toCvShare(right_msg, sensor_msgs::image_encodings::BGR8);

    end = std::chrono::steady_clock::now();
    ROS_DEBUG("VideoRecorder: toCvShare for both images: elapsed time in nsec: %ld",
              std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count());
    if (left_cv_ptr->image.cols != right_cv_ptr->image.cols ||
        left_cv_ptr->image.rows != right_cv_ptr->image.rows)
    {
      ROS_ERROR("VideoRecorder: Left and right cameras are not the "
                "correct resolution!");
    }

    begin = std::chrono::steady_clock::now();
    // Combine right image onto left one
    int seperation = 225;
    int half_width = 1686;
    cv::hconcat(left_cv_ptr->image(cv::Rect(0, 0, half_width, 1080)), right_cv_ptr->image(cv::Rect(seperation, 0, half_width, 1080)), output);
    end = std::chrono::steady_clock::now();

    ROS_DEBUG("VideoRecorder: hconcat: elapsed time in nsec: %ld",
              std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count());
 
    begin = std::chrono::steady_clock::now();
    // Scale to half size
    cv::resize(output, output, cv::Size(0, 0), 0.6667, 0.6667, cv::INTER_NEAREST);
    ROS_INFO_THROTTLE(1.0, "Output image: %d x %d", output.size().width, output.size().height);


    if ((end - last_pub_)>=std::chrono::seconds(1)) {
    cv::resize(output, output2, cv::Size(0, 0), 0.3333, 0.3333, cv::INTER_NEAREST);
    cv_bridge::CvImage out_msg = cv_bridge::CvImage(left_msg->header,
                                                    sensor_msgs::image_encodings::BGR8, output2);

    // Publish!
     
    combined_pub_.publish(out_msg.toImageMsg()); 
    last_pub_ =  std::chrono::steady_clock::now();
  }

 
    ROS_DEBUG("VideoRecorder: resize: elapsed time in nsec: %ld",
              std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count());

    WriteFrame(output);
  }
  catch (const cv_bridge::Exception& e)
  {
    ROS_ERROR("VideoRecorder: Exception raised: %s", e.what());
    ROS_ERROR("VideoRecorder: Could not convert from '%s' to 'bgr8'",
              right_msg->encoding.c_str());
  }

  end = std::chrono::steady_clock::now();
  ROS_INFO_THROTTLE(1.0, "ImagesCallback: %f", 
                    static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(end - fn_start).count())/1000000.0);


}

