/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 *
 * Copyright 2017, Avidbots Corp.
 * @name  video_recorder_node
 * @brief Video recorder standalone node
 * @author  Joseph Duchesne, Yoohee Choi
 */

#include <ctime>
#include <string>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

#include <ros/ros.h>
#include "avidbots_camera_merge/video_recorder.h"

int main(int argc, char **argv)
{
  /* Init node */
  ros::init(argc, argv, "avidbots_video_recorder");

  /* Make directory if does not exist. */
  std::string path = "/var/spool/avidbots/video_log/";
  mkdir(path.c_str(), 0777);

  DIR* dir = opendir(path.c_str());

  if (dir)
  {
    /* Start streaming */
    ros::NodeHandle nh;
    VideoRecorder output(path, nh);

    ros::spin();
    closedir(dir);
  }
  else
  {
    ROS_ERROR("VideoRecorder: Failed to create directory "
              "%s %s", path.c_str(), strerror(errno));
  }
}
