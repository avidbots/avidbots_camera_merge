/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2016, Avidbots Corp.
 * @name  image_streamer
 * @brief streams color image and generates/saves a video.
 * @author  Jeremy Wang, Yoohee Choi
 */

#include <ctime>
#include <string>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>

#include <ros/ros.h>
#include <image_transport/image_transport.h>


class Video
{
 private:
  cv::VideoWriter output_;
  bool new_clip_;
  int num_frames_;
  std::string camera_, path_;
  double max_frames_, fps_;

 public:
  /**
   * @name      Video
   * @brief     Constructor
   * @param[in] cam: left/right indicating which camera is being used
   * @param[in] max_frames: maximum number of frames
   * @param[in] fps: frame per second
   * @param[in] path: file path to where the vidoes to be stored.
   */
  Video(const std::string& cam, const double& max_frames, const double& fps,
        const std::string& path)
  {
    camera_ = cam;
    max_frames_ = max_frames;
    fps_ = fps;
    path_ = path;
    new_clip_ = true;
  }

  /**
   * @name      WriteFrame
   * @brief     Write frame to video
   * @param[in] msg - ros Image msg
   */
  void WriteFrame(const cv::Mat& img)
  {
    if (new_clip_)
    {
      num_frames_ = 0;
      new_clip_ = false;

      /* Get current time in UTC */
      char buff[100];
      time_t start = time(0);
      strftime(buff, 100, "UTC%Y%m%d_%H%M%S", gmtime(&start));

      /* Set up output */
      std::string filename(buff);
      filename = path_ + filename + "_" + camera_ + ".avi";
      output_.open(filename, CV_FOURCC('M', 'J', 'P', 'G'), fps_, img.size(), true);
    }

    if (output_.isOpened())
    {
      num_frames_++;
      output_.write(img);
    }
    else
    {
      ROS_ERROR_THROTTLE(3.0, "ImageStreamer: VideoWriter is not opened.");
    }

    if (num_frames_ > max_frames_)
    {
      new_clip_ = true;
    }
  }
};

/**
 * @name       ImageCallback
 * @brief      Receive subscribed image and write frame to video
 * @param[in]  msg - ros Image msg
 * @param[out] output - the video object that is being written to
 */
void ImageCallback(const sensor_msgs::ImageConstPtr& msg, Video& output)
{
  try
  {
    cv::Mat raw = cv_bridge::toCvShare(msg, "bgr8")->image;
    output.WriteFrame(raw);
  }
  catch (const cv_bridge::Exception& e)
  {
    ROS_ERROR("ImageStreamer: Exception raised: %s", e.what());
    ROS_ERROR("Could not convert from '%s' to 'bgr8'", msg->encoding.c_str());
  }
}

int main(int argc, char **argv)
{
  /* Init node */
  ros::init(argc, argv, "image_streamer");
  ros::NodeHandle nh;
  ros::NodeHandle n("~");

  /* Initialize parameters */
  int clip_length;
  double fps;
  std::string camera;
  n.getParam("camera", camera);
  nh.param(camera + "/driver/color_fps", fps, 30.0);
  nh.param("/clip_length", clip_length, 30);

  ROS_WARN("ImageStreamer: fps: %.2f, clip_length: %d", fps, clip_length);
  double max_frames = clip_length * 60 * fps;

  /* Make directory if does not exist. */
  std::string path = "/var/spool/avidbots/video_log/";
  mkdir(path.c_str(), 0777);

  DIR* dir = opendir(path.c_str());

  if (dir)
  {
    /* Start streaming */
    Video output(camera, max_frames, fps, path);
    image_transport::ImageTransport it(nh);
    image_transport::Subscriber sub =
                      it.subscribe(camera + "/rgb/image_raw",
                      10, boost::bind(ImageCallback, _1, output));
    ros::spin();
    closedir(dir);
  }
  else
  {
    ROS_ERROR("ImageStreamer: Failed to create directory "
              "%s %s", path.c_str(), strerror(errno));
  }
}
