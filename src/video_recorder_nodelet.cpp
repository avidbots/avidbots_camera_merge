/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2017, Avidbots Corp.
 * @name  video_recorder_nodelet.cpp
 * @brief Nodelet implementation for video recording
 * @author  Joseph Duchesne
 */


#include <pluginlib/class_list_macros.h>
#include "avidbots_camera_merge/video_recorder_nodelet.h"
#include "avidbots_camera_merge/video_recorder.h"
#include <memory>

// watch the capitalization carefully
PLUGINLIB_EXPORT_CLASS(avidbots_camera_merge::VideoRecorderNodelet, nodelet::Nodelet) 

namespace avidbots_camera_merge
{
  void VideoRecorderNodelet::onInit() 
  {
    NODELET_INFO("Initializing video recorder nodelet...");
    recorder_ = std::make_shared<VideoRecorder>("/var/spool/avidbots/video_log/", getNodeHandle());
    NODELET_INFO("VideoRecorder class instantiated");
  }
}

