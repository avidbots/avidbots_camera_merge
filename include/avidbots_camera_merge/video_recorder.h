/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2017, Avidbots Corp.
 * @name  video_recorder
 * @brief streams color image and generates/saves a video
 * @author  Yoohee Choi, Joseph Duchesne
 */

#ifndef AVIDBOTS_CAMERA_MERGE_VIDEO_RECORDER_H
#define AVIDBOTS_CAMERA_MERGE_VIDEO_RECORDER_H

#include <ctime>
#include <string>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <chrono>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/CompressedImage.h>
#include <message_filters/subscriber.h>
#include <nodelet/nodelet.h>

#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>


class VideoRecorder
{
 private:
  cv::VideoWriter output_;
  bool new_clip_;
  int num_frames_, fps_;
  std::string path_;
  double max_frames_;
  std::chrono::steady_clock::time_point start_;

  image_transport::ImageTransport image_transport_;
  image_transport::Publisher combined_pub_;
  std::chrono::steady_clock::time_point last_pub_;
  
  typedef message_filters::Subscriber<sensor_msgs::Image> ImageSubscriber;

  ImageSubscriber left_sub_;
  ImageSubscriber right_sub_;

  typedef message_filters::sync_policies::ApproximateTime
    <sensor_msgs::Image, sensor_msgs::Image> CameraSyncPolicy;

  message_filters::Synchronizer<CameraSyncPolicy> sync_;

 public:
  /**
   * @name      Video
   * @brief     Constructor
   * @param[in] path: file path to where the vidoes to be stored.
   */
  VideoRecorder(const std::string& path, ros::NodeHandle nodehandle_);

  /**
   * @name      WriteFrame
   * @brief     Write frame to video
   * @param[in] msg - combined image msg (L/R horzontally concatenated)
   */
  void WriteFrame(const cv::Mat& img);

  /**
   * @name       ImagesCallback
   * @brief      Called by the synchronizer message filter when it sync two images (L/R).
   * @param[in]  left_msg: left camera image
   * @param[in]  right_msg: right camera image
   */
  void ImagesCallback(const sensor_msgs::ImageConstPtr& left_msg,
                      const sensor_msgs::ImageConstPtr& right_msg);

};

#endif
