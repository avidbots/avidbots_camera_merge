/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2017, Avidbots Corp.
 * @name  video_recorder_nodelet.h
 * @brief Video recorder nodelet class
 * @author  Yoohee Choi, Joseph Duchesne
 */

#ifndef AVIDBOTS_CAMERA_MERGE_VIDEO_RECORDER_NODELET_H
#define AVIDBOTS_CAMERA_MERGE_VIDEO_RECORDER_NODELET_H

#include <nodelet/nodelet.h>
#include "avidbots_camera_merge/video_recorder.h"
#include <memory>

namespace avidbots_camera_merge
{
  std::shared_ptr<VideoRecorder> recorder_;

  class VideoRecorderNodelet : public nodelet::Nodelet
  {
    public:
      virtual void onInit();
  };

}

#endif
