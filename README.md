# README #

Version 0.1 - Initial release

### What is this repository for? ###

Avidbots camera info is a ROS node that does the following:

* Merges two 30fps 1080p ROS Image message streams into one camera feed.
* Saves the combined stream to disk at 30fps (using the x264 library)
* Re-publishes the combined video stream at 1Hz

### How do I get set up? ###

* Install gstreamer, ros-indigo or kinetic, intel vaapi drivers, x264, and gstreamer plugins required to use vaapi drivers
* Create catkin workspace
* Check out this repo into your catkin workspace's src folder
* catkin_make

# License Information #

This software is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  The authors allow the users to use and modify the
source code for their own research. Any commercial application, redistribution,
etc has to be arranged between users and authors individually.

avidbots_camera_merge is licenced under GPL v.2.